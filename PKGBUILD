# Maintainer: Muflone http://www.muflone.com/contacts/english/
 
pkgname=4kvideodownloader
pkgver=4.18.4.4550
pkgrel=1
pkgdesc="4K Video Downloader allows downloading videos, playlists, channels and subtitles from YouTube, Facebook, Vimeo and other video sites in high quality"
arch=('x86_64')
url="https://www.4kdownload.com/products/product-videodownloader"
license=('custom:eula')
makedepends=('chrpath' 'imagemagick')
#install=${pkgname}.install
source=("${pkgname}_$( cut -f1-3 -d'.' <<< ${pkgver%.*} )_amd64.tar.bz2"::"https://dl.4kdownload.com/app/${pkgname}_$( cut -f1-3 -d'.' <<< ${pkgver%.*} )_amd64.tar.bz2"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png"
        "fix_symlink_path.patch")
sha256sums=('8fcb6bbb4447c6af375636a13901344c4d41b607839162c26962d71f4522f443'
            '6eb02f20005d64e4eb4ec6a08e5d131adc1e0e3c0c0155e8fed31fdb6d4e221c'
            'c595b18a83b3804202b4d341a1b7a68ff3f50e33522fbc215c6537dff0adae77'
            'a4754299118c6e110bd4d248d9da0445bf350aa6954fcd51a80708d8f2c6f0d2')
 
prepare() {
    cd "${pkgname}"
    # Remove insecure RPATH
    chrpath --delete "${pkgname}-bin"

    # Fix symlink path
    patch -p1 -i "${srcdir}/fix_symlink_path.patch"
}

_4kvideodownloader_desktop="[Desktop Entry]
Name=4K Video Downloader
GenericName=4K Video Downloader
Comment=Download online video
Comment[pt_BR]=Baixar vídeo online
Exec=4kvideodownloader
Terminal=false
Type=Application
Icon=4kvideodownloader.png
Categories=AudioVideo;Network;Qt;"

build() {
    cd "${srcdir}"
    echo -e "$_4kvideodownloader_desktop" | tee com.${pkgname}.desktop
}
 
package() {
    depends=('openssl')

    # Install files
    install -m 755 -d "${pkgdir}/usr/lib"
    cp -r "${pkgname}" "${pkgdir}/usr/lib"
    chown root.root "${pkgdir}/usr/lib/${pkgname}"

    # Install launcher file
    install -m 755 -d "${pkgdir}/usr/bin"
    ln -s "/usr/lib/${pkgname}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"

    # Install license file
    install -m 755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
    install -m 644 -t "${pkgdir}/usr/share/licenses/${pkgname}" "${pkgname}/doc/eula"
    
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
} 
